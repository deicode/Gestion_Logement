﻿//Load Data in Table when documents is ready  
var mydatatable = null;
$(document).ready(function () {

    loadData();

});

//Load Data function  
function loadData() {
    if (mydatatable != null)
        mydatatable.destroy();

    $.ajax({
        url: "/Communes/List",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            var html = '';
            $.each(result, function (key, items) {
                html += '<tr>';
                //html += '<td style="width: 1%;">' + item.IdUser + '</td>';
                html += '<td style="text-transform:uppercase">' + items.Nom_com + '</td>';
                html += '<td style="text-transform:uppercase">' + items.Dist_agence + '</td>';
                html += '<td style="text-transform:uppercase">' + items.Nbre_habitant + '</td>';
               

                html += '<td style="width: 5%;"><button class="btn btn-primary btn-sm glyphicon glyphicon-pencil" onclick="return getbyID(' + items.IDcom + ')"></button> </td>';
                html += '<td style="width: 5%;"><button class="btn btn-danger btn-sm glyphicon glyphicon-trash waves-effect waves-light" onclick="Delete(' + items.IDcom + ')"></button> </td>';
                html += '</tr>';

            });
            $('.tbody').html(html);

            mydatatable = $("#data_post").DataTable({

                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.15/i18n/French.json"
                },
                //dom: 'lBfrtip',
                dom: 'lfr<"table-filter-container">tip',
                initComplete: function (settings) {
                    var api = new $.fn.dataTable.Api(settings);
                    $(".table-filter-container", api.table().container()).append(
                       $("#table-filter").detach().show()
                    );

                    $("#table-filter select").on("change", function () {
                        mydatatable.search(this.value).draw();
                    });
                }

                //buttons: [
                //'copyHtml5', 'csv', 'excel', 'pdf', 'print'
                //]
            });
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });


}

//Add Data Function   
function Add() {

    var res = validate();
    if (res == false) {
        return false;
    }
    var tpObj = {
        IDcom: $('#IDcom').val(),
        Nom_com: $('#Nom_com').val(),
        Dist_agence: $('#Dist_agence').val(),
        Nbre_habitant: $('#Nbre_habitant').val(),
        
    };
    $.ajax({
        url: "/Communes/Add",
        data: JSON.stringify(tpObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            loadData();
            $('#myModal').modal('hide');
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });



    //console.log("hvhgc " + fp);

}

////Function for getting the Data Based upon Employee ID  
function getbyID(tpID) {
    //$('#userLogin').css('border-color', 'green');
    //$('#userPass').css('border-color', 'green');
    //$('#list_profil').css('border-color', 'green');

    $.ajax({
        url: "/Communes/getbyID/" + tpID,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            $('#IDcom').val(result.IDcom);
            $('#Nom_com').val(result.Nom_com);
            $('#Dist_agence').val(result.Dist_agence);
            $('#Nbre_habitant').val(result.Nbre_habitant);


            $('#myModalLabel').html("<h4 class='modal-title'>Modification</h4>");

            $('#fg_validation').css('display', 'block');


            $('#myModal').modal('show');
            $('#btnUpdate').show();
            $('#btnAdd').hide();
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}

////function for updating employee's record  
function Update() {
    var res = validate();
    if (res == false) {
        return false;
    }
    var tpObj = {
        IDcom: $('#IDcom').val(),
        Nom_com: $('#Nom_com').val(),
        Dist_agence: $('#Dist_agence').val(),
        Nbre_habitant: $('#Nbre_habitant').val(),


       
    };
    $.ajax({
        url: "/Communes/Update",
        data: JSON.stringify(tpObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            loadData();
            $('#myModal').modal('hide');
            $('#IDcom').val("");
            $('#Nom_com').val("");
            $('#Dist_agence').val("");
            $('#Nbre_habitant').val("");
      

        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

////function for deleting employee's record  
function Delete(ID) {

    $("#DeleteModal").modal('show');

    $('.btnvalidersupp').on('click', function () {
        $.ajax({
            url: "/Communes/Delete/" + ID,
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (result) {
                loadData();
                $("#DeleteModal").modal('hide');
                window.location.reload();
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });

    });
    $('.btnfermer').on('click', function () { $("#DeleteModal").modal('hide'); });
}

//Function for clearing the textboxes  
function clearTextBox() {
    $('#IDcom').val("");
    $('#Nom_com').val("");
    $('#Dist_agence').val("");
    $('#Nbre_habitant').val("");

    $('#myModalLabel').html("<h4 class='modal-title'>Nouvel enregistrement</h4>");
    $('#btnUpdate').hide();
    $('#btnAdd').show();
    //$('#userLogin').css('border-color', 'lightgrey');
    //$('#userPass').css('border-color', 'lightgrey');
    //$('#list_profil').css('border-color', 'lightgrey');

}
//Valdidation using jquery  
function validate() {
    var isValid = true;
    if ($('#Nom_com,#Dist_agence,#Nbre_habitant').val().trim() == "") {
        $('#Nom_com,#Dist_agence,#Nbre_habitant').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#Nom_com,#Dist_agence,#Nbre_habitant').css('border-color', 'lightgrey');

    }

    return isValid;
}

function UploadFile() {
    //var fp0;
    // Checking whether FormData is available in browser  
    if (window.FormData !== undefined) {

        var fileUpload = $("#vignette").get(0);
        var files = fileUpload.files;

        // Create FormData object  
        var fileData = new FormData();

        // Looping over all files and add it to FormData object  
        for (var i = 0; i < files.length; i++) {
            fileData.append(files[i].name, files[i]);
        }

        // Adding one more key to FormData object  
        fileData.append('username', 'Manas');

        $.ajax({
            url: '/Lofement/UploadFiles',
            type: "POST",
            contentType: false, // Not to set any content header  
            processData: false, // Not to process data  
            data: fileData,
            success: function (result) {
                let fp0 = result.fp;
                //$('#vignette_path').val(fp0);
                document.getElementById("vignette_path").value = result.fp;
                console.log("fp = " + result.fp);
                alert(result.msg);


            },
            error: function (err) {
                alert(err.statusText);
            }
        });
    } else {
        alert("FormData is not supported.");
    }



    //fp1 = fp0;
}

var fp1;






