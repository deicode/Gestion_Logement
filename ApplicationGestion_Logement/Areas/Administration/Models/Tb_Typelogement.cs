﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApplicationGestion_Logement.Areas.Administration.Models
{
    public class Tb_Typelogement
    {
        public int IDtypelog { get; set; }
        public string libelle_type { get; set; }
        
    }
}