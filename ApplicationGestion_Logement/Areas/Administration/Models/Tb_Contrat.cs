﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gestion_log.Models
{
    public class Tb_Contrat
    {
        public int IDcontrat { get; set; }
        public string ref_contrat { get; set; }
        public string Deb_contrat { get; set; }
        public string Fin_contrat { get; set; }
        public string DateCreation { get; set; }

        public int Id_loc { get; set; }
        public string Nom_loc { get; set; }
        public string Prenoms_loc { get; set; }
        public int Id_log { get; set; }
        public string Libelle_log { get; set; }
        public int Id_Prop { get; set; }
        public string Nom_Prop { get; set; }
        public string Prenoms_Prop { get; set; }
        public string Description { get; set; }
        public string Nature { get; set; }
    }
}