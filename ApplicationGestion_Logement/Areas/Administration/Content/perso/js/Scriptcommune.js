﻿
    $('.btn.btn-primary.envoyercommune').on('click', function () {
        var nomdelacommune = $('#nom_commune').val();
        var distanceagence= $('#dist_agence').val();
        var nbrdhabitant = $('#nbr_habitant').val();
        $.ajax({
            type: 'post',
            url: '@Url.RouteUrl("Sauvegarde_route")',
            data: {
                'nomdelacommune': nomdelacommune,
                'distanceagence': distanceagence,
                'nbrdhabitant': nbrdhabitant
            },

            success: function (reponse) {
                $("#AjouterModal").modal('hide');
                window.location.reload()
            },

            error: function () {

            },

        })


    });


        $('.btn.btn-primary.btn-sm.glyphicon.glyphicon-pencil.modifiercommune').on('click', function () {
            var Idcommune = $(this).prop("id"); //permet de recuper la valeur du id 
            $.ajax({
                type: 'get',
                url: '@Url.RouteUrl("Modifier_route")',
                data: {
                    'Idcommune': Idcommune                
                },

                success: function (reponse) {
                    var data = reponse;
                    var nomcomune = data.nomCommune;
                    var distance = data.distance;
                    var nombre_habit = data.nombre_habit;
                    var id = data.id;
                
                    $("#nom_communemodif").val(nomcomune);
                    $("#dist_agencemodif").val(distance);
                    $("#nbr_habitantmodif").val(nombre_habit);
                    $("#Id_communemodif").val(id);

                    $("#ModifModal").modal("show");
                },

                error: function () {

                },

            })


        });


    $('.btn.btn-primary.envoyercommunemodif1').on('click', function () {
        var nomdelacommunemodif= $("#nom_communemodif").val();
        var distanceagencemodif= $("#dist_agencemodif").val();
        var nbrdhabitantmodif = $("#nbr_habitantmodif").val(); 
        var Idcommunemodif= $("#Id_communemodif").val();
        $.ajax({
            type: 'post',
            url: '@Url.RouteUrl("Sauvegardemodif_route")',
            data: {
                'nomdelacommunemodif': nomdelacommunemodif,
                'distanceagencemodif': distanceagencemodif,
                'nbrdhabitantmodif': nbrdhabitantmodif,
                'Idcommunemodif': Idcommunemodif                
            },

            success: function (reponse) {
                $("#ModifModal").modal('hide');
                window.location.reload()
            },

            error: function () {

            },

        })


    });


       $('.supprimercommune').on('click', function () {
           var Idcommunesupp = $(this).prop("id");
           $('#Id_communesupprimer').val(Idcommunesupp);
           $('#DeleteModal').modal('show');
       });

        $('.btnvalidersupp').on('click', function () {
            var Idcommunevalidersupp = $('#Id_communesupprimer').val();
            $.ajax({
                type: 'post',
                url: '@Url.RouteUrl("Validersuppresion_route")',
                data: {
                    'Idcommunevalidersupp': Idcommunevalidersupp                    
                },

                success: function (reponse) {
                    $("#DeleteModal").modal('hide');
                    window.location.reload()
                },

                error: function () {

                },

            })
           
        });

    $('.btnfermer').on('click', function () {
        $('#DeleteModal').modal('hide');
    });
