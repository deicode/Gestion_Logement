﻿using System;
using ApplicationGestion_Logement.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Gestion_log.Models;
using ApplicationGestion_Logement.Areas.Administration.Models;

namespace ApplicationGestion_Logement.Areas.Administration.Controllers
{
    public class LocatairesController : Controller
    {
        private location_bdEntities db = new location_bdEntities();
        string message = string.Empty;
        // GET: Locataires
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult List()
        {
            var loc = db.Locataire;
            List<Tb_Locataire> items = new List<Tb_Locataire>();
            foreach (var item in loc)
            {
                items.Add(new Tb_Locataire
                {
                    IDloc = item.IDloc,
                    Nom_loc = item.Nom_loc,
                    Prenom_loc = item.Prenom_loc,
                    Naiss_loc = string.Format("{0:dd-MM-yyyy}", item.Naiss_loc),
                    Contact_loc = item.Contact_loc
                });
            }


            return Json(items.ToList(), JsonRequestBehavior.AllowGet);

        }
        public JsonResult Add(Locataire item)
        {
            try
            {
                string login;

                Random rand = new Random();
                rand.Next();
                // Generate random integer from 1 to 1000.
                int r = rand.Next(1, 1000);

                login = item.Nom_loc + item.Prenom_loc + "_" + r;
                item.IDloc = 0;
                item.Id_Profil = 2;
                var utilisateur = db.Locataire.Where(e => e.Email == item.Email).SingleOrDefault();
                if (utilisateur == null)
                {
                    db.Locataire.Add(item);
                    db.SaveChanges();

                    System.Net.Mail.MailMessage m = new System.Net.Mail.MailMessage(
                                new System.Net.Mail.MailAddress("immoblierplusci@gmail.com", "Immobilier Plus"),
                                new System.Net.Mail.MailAddress(item.Email));
                    m.Subject = "Immobilier Plus - Confirmation email";
                    m.Body = string.Format("Cher {0},<BR/>Merci de votre inscription. <BR/><b>Votre nom utilisateur est:\n {1}</b><br/> Veuillez cliquer sur ce lien pour l'ouverture de votre compte sur notre platforme:<a href=\"{2}\" title=\"Confirmation email\">{2}</a>",  item.Nom_loc + " " + item.Prenom_loc, item.Email, Url.Action("ConfirmEmail", "Locataires", new { Token = item.IDloc, Email = item.Email }, Request.Url.Scheme));
                    m.IsBodyHtml = true;
                    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com");
                    smtp.Credentials = new System.Net.NetworkCredential("immoblierplusci@gmail.com", "immobilier2018");
                    smtp.EnableSsl = true;
                    smtp.Send(m);

                    message = "INSCRIPTION_SUCCES";
                    ViewBag.Message = message;
                }


            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(int Token, string Email)
        {
             location_bdEntities db = new location_bdEntities();
          
            Locataire user = db.Locataire.Where(e => e.IDloc == Token).FirstOrDefault();

            if (user != null)
            {
                if (user.IDloc == Token)
                {
                    db.Utilisateur.Add(new Utilisateur
                    {
                        login = user.Email,
                        Mpuser = "",
                        Non_user = user.Nom_loc,
                        Prenoms_user = user.Prenom_loc,                       
                        Id_Profil = 2
                    });
                    db.SaveChanges();
                    //await Manager.UpdateAsync(user);
                    //await SignInAsync(user, isPersistent: false);
                    return RedirectToAction("ChangePassword", "Utilisateur", new { Email = user.Email });
                }
                else
                {
                    return HttpNotFound();
                }
            }
            else
            {
                return HttpNotFound();
            }
        }
      

        public JsonResult GetbyID(int ID)
        {
            var loc = db.Locataire.Where(e => e.IDloc == ID).SingleOrDefault();
            if (loc != null)
            {
                var item = new Tb_Locataire
                {
                    IDloc = loc.IDloc,
                    Nom_loc = loc.Nom_loc,
                    Prenom_loc = loc.Prenom_loc,
                    Naiss_loc =  string.Format("{0:dd-MM-yyyy}", loc.Naiss_loc),
                    Contact_loc = loc.Contact_loc,
                    Email = loc.Email

                };

                return Json(item, JsonRequestBehavior.AllowGet);
            }

            else
                return Json(new { action = false }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Update(Locataire item)
        {
            var loc = db.Locataire.Where(e => e.IDloc == item.IDloc).SingleOrDefault();
            try
            {
                loc.Nom_loc = item.Nom_loc;
                loc.Prenom_loc = item.Prenom_loc;
                loc.Naiss_loc =  item.Naiss_loc;
                loc.Contact_loc = item.Contact_loc;
                loc.Email = item.Email;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int ID)
        {
            var loc = db.Locataire.Where(e => e.IDloc == ID).SingleOrDefault();
            try
            {
                db.Locataire.Remove(loc);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }
    }
}
