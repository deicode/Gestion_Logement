﻿using ApplicationGestion_Logement.Models;
using Gestion_log.Models;


using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ApplicationGestion_Logement.Areas.Administration.Controllers
{
    public class CommunesController : Controller
    {
        private location_bdEntities db = new location_bdEntities();
        // GET: Home  
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult List()
        {
            var log = db.Commune;
            List<Tb_Commune> items = new List<Tb_Commune>();
            foreach (var item in log)
            {
                items.Add(new Tb_Commune
                {
                    IDcom = item.IDcom,
                    Nom_com = item.Nom_com,
                    Dist_agence = item.Dist_agence,
                    Nbre_habitant = item.Nbre_habitant,
                   
                    


                });
            }


            return Json(items.ToList(), JsonRequestBehavior.AllowGet);

        }
        public JsonResult Add(Commune item)
        {
            try
            {
                
                db.Commune.Add(item);
                
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetbyID(int ID)
        {
            var log = db.Commune.Where(e => e.IDcom == ID).SingleOrDefault();
            if (log != null)
            {
                var item = new Tb_Commune
                {
                    IDcom = log.IDcom,
                    Nom_com = log.Nom_com,
                    Dist_agence = log.Dist_agence,
                    Nbre_habitant = log.Nbre_habitant

                };




                return Json(item, JsonRequestBehavior.AllowGet);
            }

            else
                return Json(new { action = false }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Update(Commune item)
        {
            var post = db.Commune.Where(e => e.IDcom == item.IDcom).SingleOrDefault();
            try
            {
                post.IDcom = item.IDcom;
                post.Nom_com = item.Nom_com;
                post.Dist_agence = item.Dist_agence;
                post.Nbre_habitant = item.Nbre_habitant;
                
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int ID)
        {
            var post = db.Commune.Where(e => e.IDcom == ID).SingleOrDefault();
            try
            {
                db.Commune.Remove(post);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }
    }
}
