﻿using ApplicationGestion_Logement.Models;
using Gestion_log.Models;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ApplicationGestion_Logement.Areas.Administration.Controllers
{
    public class LogementsController : Controller
    {
        private location_bdEntities db = new location_bdEntities();

        
        // GET: Home  
        public ActionResult Index()
        {
            return View();
        }
     
        public JsonResult List()
        {
            var log = db.Logement.Where(e => e.Is_Delete == false);
            List<Tb_Logement> items = new List<Tb_Logement>();
            foreach (var item in log)
            {
                items.Add(new Tb_Logement
                {
                    IDlog = item.IDlog,
                    Titre= item.Titre,
                    Id_Typelog = item.Type_logement.IDtypelog,
                    Libelle_typelog = item.Type_logement.libelle_type,
                    Superficie= item.Superficie,
                    Id_Quartier = item.Quartier.IDQuartier,
                    Libelle_quartier = item.Quartier.Nom_quartier,
                    Is_validate = item.Is_validate.Value,


                });
            }


                return Json(items.ToList(), JsonRequestBehavior.AllowGet);

        }
        public JsonResult Add(Logement item)
        {
            try
            {
                item.Is_Delete = false;
                item.Is_validate = false;
                db.Logement.Add(item);
               
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetbyID(int ID)
        {
            var log = db.Logement.Where(e => e.IDlog == ID).SingleOrDefault();
            if (log != null)
            {
                var item = new Tb_Logement
                {
                    IDlog = log.IDlog,
                    Titre = log.Titre,
                    Id_Typelog = log.Type_logement.IDtypelog,
                    Libelle_typelog = log.Type_logement.libelle_type,
                    Superficie = log.Superficie,
                    Id_Quartier = log.Quartier.IDQuartier,
                    Libelle_quartier = log.Quartier.Nom_quartier,
                    MATlog = log.MATlog,
                    Desclog = log.Desclog,
                    CordX = log.CordX.Value,
                    CordY = log.CordY.Value,
                    Adrlog = log.Adrlog,
                    Id_statut = log.Statut.Id_statut,
                    Libelle_statut= log.Statut.Lib_statut,
                    Prix_location = !string.IsNullOrWhiteSpace(log.Prix_location.ToString()) ? Convert.ToInt32(log.Prix_location) : 0 ,
                    Is_validate = log.Is_validate.Value,

                };
               
         
          

                return Json(item, JsonRequestBehavior.AllowGet);
            }

            else
                return Json(new { action = false }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Update(Logement item)
        {
            var post = db.Logement.Where(e => e.IDlog == item.IDlog).SingleOrDefault();
            try
            {
                post.Titre = item.Titre;
                post.Id_Typelog = item.Id_Typelog;
                post.Superficie = item.Superficie;
                post.Id_Quartier = item.Id_Quartier;
                post.MATlog = item.MATlog;
                post.Desclog = item.Desclog;
                post.CordX = item.CordX;
                post.CordY = item.CordY;
                post.Adrlog = item.Adrlog;
                post.Id_statut = item.Id_statut;
                post.Is_validate = item.Is_validate;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int ID)
        {
            var post = db.Logement.Where(e => e.IDlog == ID).SingleOrDefault();
            try
            {
                post.Is_Delete = true;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }


       
    }
}