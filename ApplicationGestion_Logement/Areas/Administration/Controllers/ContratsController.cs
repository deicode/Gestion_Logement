﻿using ApplicationGestion_Logement.Models;
using Gestion_log.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ApplicationGestion_Logement.Areas.Administration.Controllers
{
    public class ContratsController : Controller
    {
        private location_bdEntities db = new location_bdEntities();
        // GET: Contrat
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult List()
        {
            var Contrat = db.Contrat.Where(e => e.Is_Delete == false);
            List<Tb_Contrat> items = new List<Tb_Contrat>();
            foreach (var item in Contrat)
            {
                items.Add(new Tb_Contrat
                {
                    IDcontrat = item.IDcontrat,
                    ref_contrat = item.ref_contrat,
                    Deb_contrat = string.Format("{0:dd-MM-yyyy}", item.Deb_contrat),
                    Fin_contrat = string.Format("{0:dd-MM-yyyy}", item.Fin_contrat)            

                });
            }


            return Json(items.ToList(), JsonRequestBehavior.AllowGet);

        }
        public JsonResult Add(Contrat item)
        {
            try
            {
               
                item.DateCreation = DateTime.Now;
                item.Is_Delete = false;
                //item.Is_validate = true;
                db.Contrat.Add(item);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetbyID(int ID)
        {
            var prof = db.Contrat.Where(e => e.IDcontrat == ID).SingleOrDefault();
            if (prof != null)
            {
                var item = new Tb_Contrat
                {
                    IDcontrat = prof.IDcontrat,
                    ref_contrat = prof.ref_contrat,
                    Deb_contrat = string.Format("{0:dd-MM-yyyy}", prof.Deb_contrat),
                    Fin_contrat = string.Format("{0:dd-MM-yyyy}", prof.Fin_contrat),
                    Id_loc= prof.Id_loc.Value,
                    Id_log = prof.Id_log.Value,
                    Id_Prop = prof.Id_Prop.Value,
                    Description= prof.Description,
                    Nature = prof.Nature,
                };
                return Json(item, JsonRequestBehavior.AllowGet);
            }

            else
                return Json(new { action = false }, JsonRequestBehavior.AllowGet);
        }
        //public JsonResult Update(Contrat item)
        //{
        //    var prof = db.Contrat.Where(e => e.IDcontrat == item.IDcontrat).SingleOrDefault();
        //    try
        //    {
        //        prof.Nom_Contrat = item.Nom_Contrat;
        //        prof.Prenoms_Contrat = item.Prenoms_Contrat;
        //        prof.Num = item.Num;
        //        prof.Email = item.Email;

        //        db.SaveChanges();
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
        //    }
        //    return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        //}
        public JsonResult Delete(int ID)
        {
            var prof = db.Contrat.Where(e => e.IDcontrat == ID).SingleOrDefault();
            try
            {
                prof.Is_Delete = true;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }

    }
}