﻿using ApplicationGestion_Logement.Models;
using Gestion_log.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ApplicationGestion_Logement.Areas.Administration.Controllers
{
    public class ProprietaireController : Controller
    {
        private location_bdEntities db = new location_bdEntities();
        // GET: Proprietaire
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult List()
        {
            var prop = db.Proprietaire.Where(e => e.Is_Delete == false);
            List<Tb_Proprietaire> items = new List<Tb_Proprietaire>();
            foreach (var item in prop)
            {
                items.Add(new Tb_Proprietaire
                {
                    ID = item.ID,
                    Nom_prop = item.Nom_prop,
                    Prenoms_prop = item.Prenoms_prop,
                    Num = item.Num,
                    Email = item.Email,

                });
            }


            return Json(items.ToList(), JsonRequestBehavior.AllowGet);

        }
        public JsonResult Add(Proprietaire item)
        {
            try
            {
                item.Is_Delete = false;
                item.Id_Profil = 1;
                item.DateEnregistrement = DateTime.Now;
                //item.Is_validate = true;
                db.Proprietaire.Add(item);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetbyID(int ID)
        {
            var prof = db.Proprietaire.Where(e => e.ID == ID).SingleOrDefault();
            if (prof != null)
            {
                var item = new Tb_Proprietaire
                {
                    ID = prof.ID,
                    Nom_prop = prof.Nom_prop,
                    Prenoms_prop = prof.Prenoms_prop,
             Num = prof.Num,
                Email = prof.Email,


            };




                return Json(item, JsonRequestBehavior.AllowGet);
            }

            else
                return Json(new { action = false }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Update(Proprietaire item)
        {
            var prof = db.Proprietaire.Where(e => e.ID == item.ID).SingleOrDefault();
            try
            {
                prof.Nom_prop = item.Nom_prop;
                prof.Prenoms_prop = item.Prenoms_prop;
                prof.Num = item.Num;
                prof.Email = item.Email;

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int ID)
        {
            var prof = db.Proprietaire.Where(e => e.ID == ID).SingleOrDefault();
            try
            {
                prof.Is_Delete = true;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }

    }
}