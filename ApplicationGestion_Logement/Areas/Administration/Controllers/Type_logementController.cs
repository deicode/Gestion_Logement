﻿using System;
using ApplicationGestion_Logement.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Gestion_log.Models;

namespace ApplicationGestion_Logement.Areas.Administration.Controllers
{
    public class Type_logementController : Controller
    {
        private location_bdEntities db = new location_bdEntities();

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult List()
        {
            var typelog = db.Type_logement;
            List<Tb_Typelog> items = new List<Tb_Typelog>();
            foreach (var item in typelog)
            {
                items.Add(new Tb_Typelog
                {
                    IDtypelog = item.IDtypelog,
                    libelle_type = item.libelle_type
                });
            }


            return Json(items.ToList(), JsonRequestBehavior.AllowGet);

        }
        public JsonResult Add(Type_logement item)
        {
            try
            {
               
                db.Type_logement.Add(item);
               
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetbyID(int ID)
        {
            var typelog = db.Type_logement.Where(e => e.IDtypelog == ID).SingleOrDefault();
            if (typelog != null)
            {
                var item = new Type_logement
                {
                   IDtypelog = typelog.IDtypelog,
                   libelle_type = typelog.libelle_type

                };

                return Json(item, JsonRequestBehavior.AllowGet);
            }

            else
                return Json(new { action = false }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Update(Type_logement item)
        {
            var typelog = db.Type_logement.Where(e => e.IDtypelog == item.IDtypelog).SingleOrDefault();
            try
            {
                typelog.IDtypelog = item.IDtypelog;
                typelog.libelle_type = item.libelle_type;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int ID)
        {
            var post = db.Type_logement.Where(e => e.IDtypelog == ID).SingleOrDefault();
            try
            {
                db.Type_logement.Remove(post);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { action = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { action = true }, JsonRequestBehavior.AllowGet);
        }
    }
}
