
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 01/26/2018 15:30:57
-- Generated from EDMX file: D:\PROJETS\gestionlogement\ApplicationGestion_Logement\ApplicationGestion_Logement\Models\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [location_bd];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Contrat_Locataire]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Contrat] DROP CONSTRAINT [FK_Contrat_Locataire];
GO
IF OBJECT_ID(N'[dbo].[FK_Contrat_Logement]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Contrat] DROP CONSTRAINT [FK_Contrat_Logement];
GO
IF OBJECT_ID(N'[dbo].[FK_Contrat_Proprietaire]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Contrat] DROP CONSTRAINT [FK_Contrat_Proprietaire];
GO
IF OBJECT_ID(N'[dbo].[FK_Locataire_Profil]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Locataire] DROP CONSTRAINT [FK_Locataire_Profil];
GO
IF OBJECT_ID(N'[dbo].[FK_Logement_Quartier]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Logement] DROP CONSTRAINT [FK_Logement_Quartier];
GO
IF OBJECT_ID(N'[dbo].[FK_Logement_Statut]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Logement] DROP CONSTRAINT [FK_Logement_Statut];
GO
IF OBJECT_ID(N'[dbo].[FK_Logement_TypeLogement]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Logement] DROP CONSTRAINT [FK_Logement_TypeLogement];
GO
IF OBJECT_ID(N'[dbo].[FK_Proprietaire_Profil]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Proprietaire] DROP CONSTRAINT [FK_Proprietaire_Profil];
GO
IF OBJECT_ID(N'[dbo].[FK_Proprietaire_Proprietaire]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Proprietaire] DROP CONSTRAINT [FK_Proprietaire_Proprietaire];
GO
IF OBJECT_ID(N'[dbo].[FK_Quartier_Commune]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Quartier] DROP CONSTRAINT [FK_Quartier_Commune];
GO
IF OBJECT_ID(N'[dbo].[FK_Rapport_sortie_Proprietaire]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Rapport_sortie] DROP CONSTRAINT [FK_Rapport_sortie_Proprietaire];
GO
IF OBJECT_ID(N'[dbo].[FK_Requete_Utilisateur]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Requete] DROP CONSTRAINT [FK_Requete_Utilisateur];
GO
IF OBJECT_ID(N'[dbo].[FK_Reservation_Locataire]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Reservation] DROP CONSTRAINT [FK_Reservation_Locataire];
GO
IF OBJECT_ID(N'[dbo].[FK_Reservation_Logement]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Reservation] DROP CONSTRAINT [FK_Reservation_Logement];
GO
IF OBJECT_ID(N'[dbo].[FK_Id_Profil]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Utilisateur] DROP CONSTRAINT [FK_Id_Profil];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Commune]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Commune];
GO
IF OBJECT_ID(N'[dbo].[Contrat]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Contrat];
GO
IF OBJECT_ID(N'[dbo].[Locataire]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Locataire];
GO
IF OBJECT_ID(N'[dbo].[Logement]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Logement];
GO
IF OBJECT_ID(N'[dbo].[Profil]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Profil];
GO
IF OBJECT_ID(N'[dbo].[Proprietaire]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Proprietaire];
GO
IF OBJECT_ID(N'[dbo].[Quartier]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Quartier];
GO
IF OBJECT_ID(N'[dbo].[Rapport_sortie]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Rapport_sortie];
GO
IF OBJECT_ID(N'[dbo].[Requete]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Requete];
GO
IF OBJECT_ID(N'[dbo].[Reservation]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Reservation];
GO
IF OBJECT_ID(N'[dbo].[Statut]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Statut];
GO
IF OBJECT_ID(N'[dbo].[sysdiagrams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[sysdiagrams];
GO
IF OBJECT_ID(N'[dbo].[Type_logement]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Type_logement];
GO
IF OBJECT_ID(N'[dbo].[Utilisateur]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Utilisateur];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Commune'
CREATE TABLE [dbo].[Commune] (
    [IDcom] int IDENTITY(1,1) NOT NULL,
    [Nom_com] varchar(max)  NOT NULL,
    [Dist_agence] int  NOT NULL,
    [Nbre_habitant] int  NOT NULL
);
GO

-- Creating table 'Contrat'
CREATE TABLE [dbo].[Contrat] (
    [IDcontrat] int IDENTITY(1,1) NOT NULL,
    [ref_contrat] nchar(10)  NULL,
    [Deb_contrat] datetime  NULL,
    [Fin_contrat] datetime  NULL,
    [DateCreation] datetime  NULL,
    [Is_Delete] bit  NULL,
    [Id_loc] int  NULL,
    [Id_log] int  NULL,
    [Id_Prop] int  NULL,
    [Description] nvarchar(max)  NULL,
    [Nature] nvarchar(50)  NULL
);
GO

-- Creating table 'Locataire'
CREATE TABLE [dbo].[Locataire] (
    [IDloc] int IDENTITY(1,1) NOT NULL,
    [Nom_loc] nchar(10)  NULL,
    [Prenom_loc] nchar(10)  NULL,
    [Naiss_loc] datetime  NULL,
    [Contact_loc] nvarchar(50)  NULL,
    [Id_Profil] int  NULL,
    [Email] nchar(10)  NULL
);
GO

-- Creating table 'Logement'
CREATE TABLE [dbo].[Logement] (
    [IDlog] int IDENTITY(1,1) NOT NULL,
    [MATlog] nvarchar(50)  NULL,
    [charges_forfetaire] nchar(10)  NULL,
    [Superficie] nchar(10)  NULL,
    [CordX] int  NULL,
    [CordY] int  NULL,
    [Desclog] nvarchar(50)  NULL,
    [Distance] int  NULL,
    [Adrlog] nchar(10)  NULL,
    [Is_validate] bit  NULL,
    [Is_Delete] bit  NULL,
    [Id_Quartier] int  NULL,
    [Id_Typelog] int  NULL,
    [Id_statut] int  NULL,
    [Prix_location] decimal(19,4)  NULL,
    [Titre] nchar(10)  NULL,
    [Vignette] nvarchar(max)  NULL
);
GO

-- Creating table 'Profil'
CREATE TABLE [dbo].[Profil] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Lib_profil] nvarchar(max)  NULL,
    [Is_delete] bit  NULL
);
GO

-- Creating table 'Proprietaire'
CREATE TABLE [dbo].[Proprietaire] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Nom_prop] nvarchar(max)  NULL,
    [Prenoms_prop] nvarchar(max)  NULL,
    [Num] nvarchar(max)  NULL,
    [Email] nvarchar(max)  NULL,
    [Is_Delete] bit  NULL,
    [Is_validate] bit  NULL,
    [Id_Profil] int  NOT NULL,
    [DateEnregistrement] datetime  NULL
);
GO

-- Creating table 'Quartier'
CREATE TABLE [dbo].[Quartier] (
    [IDQuartier] int IDENTITY(1,1) NOT NULL,
    [Nom_quartier] nchar(10)  NULL,
    [Idcommune] int  NULL
);
GO

-- Creating table 'Rapport_sortie'
CREATE TABLE [dbo].[Rapport_sortie] (
    [Id_Rapport] nchar(10)  NOT NULL,
    [Lib_Rapport] nchar(10)  NULL,
    [Vignette_Rapport] nchar(10)  NULL,
    [Is_validate] bit  NULL,
    [Id_prop] int  NULL,
    [DateCreation] datetime  NULL
);
GO

-- Creating table 'Requete'
CREATE TABLE [dbo].[Requete] (
    [Id_req] int  NOT NULL,
    [DateCreation] datetime  NULL,
    [Objet] nchar(10)  NULL,
    [Description] nvarchar(max)  NULL,
    [Expediteur] nchar(10)  NULL,
    [Recepteur] nchar(10)  NULL,
    [Nom] nchar(10)  NULL,
    [Prenoms] nchar(10)  NULL,
    [Email] nchar(10)  NULL,
    [Id_Utilisateur] int  NULL
);
GO

-- Creating table 'Reservation'
CREATE TABLE [dbo].[Reservation] (
    [ID] nchar(10)  NOT NULL,
    [DateDemande] datetime  NULL,
    [DateRdv] datetime  NULL,
    [Id_Loc] int  NULL,
    [Id_logement] int  NULL
);
GO

-- Creating table 'Statut'
CREATE TABLE [dbo].[Statut] (
    [Id_statut] int IDENTITY(1,1) NOT NULL,
    [Lib_statut] nchar(10)  NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'Type_logement'
CREATE TABLE [dbo].[Type_logement] (
    [IDtypelog] int IDENTITY(1,1) NOT NULL,
    [libelle_type] nchar(10)  NULL
);
GO

-- Creating table 'Utilisateur'
CREATE TABLE [dbo].[Utilisateur] (
    [Id_user] int  NOT NULL,
    [Non_user] nchar(10)  NULL,
    [Prenoms_user] nchar(10)  NOT NULL,
    [login] nchar(10)  NOT NULL,
    [Mpuser] nchar(10)  NOT NULL,
    [Id_Profil] int  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [IDcom] in table 'Commune'
ALTER TABLE [dbo].[Commune]
ADD CONSTRAINT [PK_Commune]
    PRIMARY KEY CLUSTERED ([IDcom] ASC);
GO

-- Creating primary key on [IDcontrat] in table 'Contrat'
ALTER TABLE [dbo].[Contrat]
ADD CONSTRAINT [PK_Contrat]
    PRIMARY KEY CLUSTERED ([IDcontrat] ASC);
GO

-- Creating primary key on [IDloc] in table 'Locataire'
ALTER TABLE [dbo].[Locataire]
ADD CONSTRAINT [PK_Locataire]
    PRIMARY KEY CLUSTERED ([IDloc] ASC);
GO

-- Creating primary key on [IDlog] in table 'Logement'
ALTER TABLE [dbo].[Logement]
ADD CONSTRAINT [PK_Logement]
    PRIMARY KEY CLUSTERED ([IDlog] ASC);
GO

-- Creating primary key on [ID] in table 'Profil'
ALTER TABLE [dbo].[Profil]
ADD CONSTRAINT [PK_Profil]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Proprietaire'
ALTER TABLE [dbo].[Proprietaire]
ADD CONSTRAINT [PK_Proprietaire]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [IDQuartier] in table 'Quartier'
ALTER TABLE [dbo].[Quartier]
ADD CONSTRAINT [PK_Quartier]
    PRIMARY KEY CLUSTERED ([IDQuartier] ASC);
GO

-- Creating primary key on [Id_Rapport] in table 'Rapport_sortie'
ALTER TABLE [dbo].[Rapport_sortie]
ADD CONSTRAINT [PK_Rapport_sortie]
    PRIMARY KEY CLUSTERED ([Id_Rapport] ASC);
GO

-- Creating primary key on [Id_req] in table 'Requete'
ALTER TABLE [dbo].[Requete]
ADD CONSTRAINT [PK_Requete]
    PRIMARY KEY CLUSTERED ([Id_req] ASC);
GO

-- Creating primary key on [ID] in table 'Reservation'
ALTER TABLE [dbo].[Reservation]
ADD CONSTRAINT [PK_Reservation]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [Id_statut] in table 'Statut'
ALTER TABLE [dbo].[Statut]
ADD CONSTRAINT [PK_Statut]
    PRIMARY KEY CLUSTERED ([Id_statut] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [IDtypelog] in table 'Type_logement'
ALTER TABLE [dbo].[Type_logement]
ADD CONSTRAINT [PK_Type_logement]
    PRIMARY KEY CLUSTERED ([IDtypelog] ASC);
GO

-- Creating primary key on [Id_user] in table 'Utilisateur'
ALTER TABLE [dbo].[Utilisateur]
ADD CONSTRAINT [PK_Utilisateur]
    PRIMARY KEY CLUSTERED ([Id_user] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Idcommune] in table 'Quartier'
ALTER TABLE [dbo].[Quartier]
ADD CONSTRAINT [FK_Quartier_Commune]
    FOREIGN KEY ([Idcommune])
    REFERENCES [dbo].[Commune]
        ([IDcom])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Quartier_Commune'
CREATE INDEX [IX_FK_Quartier_Commune]
ON [dbo].[Quartier]
    ([Idcommune]);
GO

-- Creating foreign key on [Id_loc] in table 'Contrat'
ALTER TABLE [dbo].[Contrat]
ADD CONSTRAINT [FK_Contrat_Locataire]
    FOREIGN KEY ([Id_loc])
    REFERENCES [dbo].[Locataire]
        ([IDloc])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Contrat_Locataire'
CREATE INDEX [IX_FK_Contrat_Locataire]
ON [dbo].[Contrat]
    ([Id_loc]);
GO

-- Creating foreign key on [Id_log] in table 'Contrat'
ALTER TABLE [dbo].[Contrat]
ADD CONSTRAINT [FK_Contrat_Logement]
    FOREIGN KEY ([Id_log])
    REFERENCES [dbo].[Logement]
        ([IDlog])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Contrat_Logement'
CREATE INDEX [IX_FK_Contrat_Logement]
ON [dbo].[Contrat]
    ([Id_log]);
GO

-- Creating foreign key on [Id_Prop] in table 'Contrat'
ALTER TABLE [dbo].[Contrat]
ADD CONSTRAINT [FK_Contrat_Proprietaire]
    FOREIGN KEY ([Id_Prop])
    REFERENCES [dbo].[Proprietaire]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Contrat_Proprietaire'
CREATE INDEX [IX_FK_Contrat_Proprietaire]
ON [dbo].[Contrat]
    ([Id_Prop]);
GO

-- Creating foreign key on [Id_Profil] in table 'Locataire'
ALTER TABLE [dbo].[Locataire]
ADD CONSTRAINT [FK_Locataire_Profil]
    FOREIGN KEY ([Id_Profil])
    REFERENCES [dbo].[Profil]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Locataire_Profil'
CREATE INDEX [IX_FK_Locataire_Profil]
ON [dbo].[Locataire]
    ([Id_Profil]);
GO

-- Creating foreign key on [Id_Loc] in table 'Reservation'
ALTER TABLE [dbo].[Reservation]
ADD CONSTRAINT [FK_Reservation_Locataire]
    FOREIGN KEY ([Id_Loc])
    REFERENCES [dbo].[Locataire]
        ([IDloc])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Reservation_Locataire'
CREATE INDEX [IX_FK_Reservation_Locataire]
ON [dbo].[Reservation]
    ([Id_Loc]);
GO

-- Creating foreign key on [Id_Quartier] in table 'Logement'
ALTER TABLE [dbo].[Logement]
ADD CONSTRAINT [FK_Logement_Quartier]
    FOREIGN KEY ([Id_Quartier])
    REFERENCES [dbo].[Quartier]
        ([IDQuartier])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Logement_Quartier'
CREATE INDEX [IX_FK_Logement_Quartier]
ON [dbo].[Logement]
    ([Id_Quartier]);
GO

-- Creating foreign key on [Id_statut] in table 'Logement'
ALTER TABLE [dbo].[Logement]
ADD CONSTRAINT [FK_Logement_Statut]
    FOREIGN KEY ([Id_statut])
    REFERENCES [dbo].[Statut]
        ([Id_statut])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Logement_Statut'
CREATE INDEX [IX_FK_Logement_Statut]
ON [dbo].[Logement]
    ([Id_statut]);
GO

-- Creating foreign key on [Id_Typelog] in table 'Logement'
ALTER TABLE [dbo].[Logement]
ADD CONSTRAINT [FK_Logement_TypeLogement]
    FOREIGN KEY ([Id_Typelog])
    REFERENCES [dbo].[Type_logement]
        ([IDtypelog])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Logement_TypeLogement'
CREATE INDEX [IX_FK_Logement_TypeLogement]
ON [dbo].[Logement]
    ([Id_Typelog]);
GO

-- Creating foreign key on [Id_logement] in table 'Reservation'
ALTER TABLE [dbo].[Reservation]
ADD CONSTRAINT [FK_Reservation_Logement]
    FOREIGN KEY ([Id_logement])
    REFERENCES [dbo].[Logement]
        ([IDlog])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Reservation_Logement'
CREATE INDEX [IX_FK_Reservation_Logement]
ON [dbo].[Reservation]
    ([Id_logement]);
GO

-- Creating foreign key on [Id_Profil] in table 'Proprietaire'
ALTER TABLE [dbo].[Proprietaire]
ADD CONSTRAINT [FK_Proprietaire_Profil]
    FOREIGN KEY ([Id_Profil])
    REFERENCES [dbo].[Profil]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Proprietaire_Profil'
CREATE INDEX [IX_FK_Proprietaire_Profil]
ON [dbo].[Proprietaire]
    ([Id_Profil]);
GO

-- Creating foreign key on [Id_Profil] in table 'Utilisateur'
ALTER TABLE [dbo].[Utilisateur]
ADD CONSTRAINT [FK_Id_Profil]
    FOREIGN KEY ([Id_Profil])
    REFERENCES [dbo].[Profil]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Id_Profil'
CREATE INDEX [IX_FK_Id_Profil]
ON [dbo].[Utilisateur]
    ([Id_Profil]);
GO

-- Creating foreign key on [ID] in table 'Proprietaire'
ALTER TABLE [dbo].[Proprietaire]
ADD CONSTRAINT [FK_Proprietaire_Proprietaire]
    FOREIGN KEY ([ID])
    REFERENCES [dbo].[Proprietaire]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id_prop] in table 'Rapport_sortie'
ALTER TABLE [dbo].[Rapport_sortie]
ADD CONSTRAINT [FK_Rapport_sortie_Proprietaire]
    FOREIGN KEY ([Id_prop])
    REFERENCES [dbo].[Proprietaire]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Rapport_sortie_Proprietaire'
CREATE INDEX [IX_FK_Rapport_sortie_Proprietaire]
ON [dbo].[Rapport_sortie]
    ([Id_prop]);
GO

-- Creating foreign key on [Id_Utilisateur] in table 'Requete'
ALTER TABLE [dbo].[Requete]
ADD CONSTRAINT [FK_Requete_Utilisateur]
    FOREIGN KEY ([Id_Utilisateur])
    REFERENCES [dbo].[Utilisateur]
        ([Id_user])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Requete_Utilisateur'
CREATE INDEX [IX_FK_Requete_Utilisateur]
ON [dbo].[Requete]
    ([Id_Utilisateur]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------