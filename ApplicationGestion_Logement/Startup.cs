﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ApplicationGestion_Logement.Startup))]
namespace ApplicationGestion_Logement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
