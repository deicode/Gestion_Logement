﻿using ApplicationGestion_Logement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ApplicationGestion_Logement.Controllers
{
    public class UtilisateurController : Controller
    {
        string message = "";
        bool status = false;
        location_bdEntities db = new location_bdEntities();
        // GET: Utilisateur
        public ActionResult Index()
        {
            return View();
        }

         public ActionResult ChangePassword(string Email)
        {
            location_bdEntities db = new location_bdEntities();
            var user = db.Utilisateur.Where(p => p.login == Email).FirstOrDefault();

            //ViewBag.Email = Email;
            if (user != null)
                return View(user);
            else
                return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(string Email, Utilisateur c)
        {
            var v = db.Utilisateur.Where(a => a.login.Equals(Email)).FirstOrDefault();
            if (v != null)
            {
                v.Mpuser = c.Mpuser;             

            }
            else
            {
                return HttpNotFound();
            }
           

            db.SaveChanges();
            status = true;
            message = "Mot de passe pris en compte.";

            ViewBag.Message = message;

            return View(c);
        }

       
    }
}